<?php
	require_once 'lg-component-menu.php';

	/**** MAIN ****/
	$GLOBALS['lg_main_menu'] = 'lg-settings';
	//define components
	$GLOBALS['lg_component'] = array(
		/**** REFERENCE LG-includes/lg-component.php ****/
		new LG_component('Google Map', 'lg-map', 'LG-map/main.php'),
		new LG_component('Image Comparison', 'lg-image-comparison', 'LG-image-comparison/main.php'),
		new LG_component('Testimonial', 'lg-testimonial', 'LG-testimonial/main.php'),
	);

    foreach ($GLOBALS['lg_component'] as $component) {
    	//Plugin Component check active
		if(get_option($component->component_slug) == 1){
			require_once $component->component_path;
		}
    }
?>
