<?php

function lg_menu() {
    add_menu_page(
        __( 'LG Components', 'lg-menu' ),
        'LG Components',
        'manage_options',
        'lg-settings',
        'lg_settings_page',
        'dashicons-networking',
        3
    );
    add_action( 'admin_init', 'lg_component_register_settings' );
    require_once 'lg-component-settings.php';
}
add_action( 'init', 'lg_menu' );
?>