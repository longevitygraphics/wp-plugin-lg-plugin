<?php
    function lg_component_register_settings(){
        /*********
            Register component settings
        *********/
        foreach ($GLOBALS['lg_component'] as $component) {
            register_setting("lg-active-component-group", $component->component_slug);
        }
        /* end */
    }
    add_action("admin_init", "lg_component_register_settings");

    function lg_settings_page() {
    ?>
        <div class="wrap">
        <h1>LG Active Component</h1>
        <form method="post" action="options.php">
        <?php
            settings_fields("lg-active-component-group");
            ?>
            <table class="form-table">
            <!-- Html for settings -->
            <?php foreach ($GLOBALS['lg_component'] as $component) { ?>
                <tr valign="top">
                    <th scope="row"><?php echo $component->component_name; ?></th>
                    <td><input type="checkbox" name="<?php echo $component->component_slug; ?>" value="1" <?php checked(1, get_option($component->component_slug), true); ?> /> 
                </tr>   
            <?php } ?>
            <!-- end -->
            </table>
            <?php submit_button(); ?>
        </form>
        </div>
    <?php }
?>