<?php

function lg_image_comparison_shortcode( $atts, $content = null ) {
    $atts = shortcode_atts( array(
        'id' => 0,
    ), $atts );

    if($atts['id'] == 0 ){
        return 'Image Comparison does not exist';
    }else{
        $post_id = $atts['id'];
        $post = get_post( $post_id );
        $slug = $post->post_name;


		wp_enqueue_style( 'lg-image-comparison-style' );
		wp_enqueue_script( 'lg-image-comparison-script' );

        ob_start();
        ?>
            <?php
                //Image source
                $image_before = get_field('lg_image_comparison_before_image', $post_id);
                $image_after = get_field('lg_image_comparison_after_image', $post_id);

                //Label text
                $image_before_label = get_field('lg_image_comparison_before_label', $post_id);
                $image_after_label = get_field('lg_image_comparison_after_label', $post_id);

                //Handle
                $handle_background = get_field('lg_image_comparison_handle_background_color', $post_id);
                $handle_background_active = get_field('lg_image_comparison_handle_background_color_active', $post_id);

                //Label
                $label_background_color = get_field('lg_image_comparison_label_background_color', $post_id);
                $label_font_color = get_field('lg_image_comparison_label_font_color', $post_id);

                //Tooltip
                $tooltip_active = get_field('lg_image_comparison_tooltip_active', $post_id);
                $tooltip_content = get_field('lg_image_comparison_tooltip_content', $post_id);
                $tooltip_background_color = get_field('lg_image_comparison_tooltip_background_color', $post_id);
                $tooltip_font_color = get_field('lg_image_comparison_tooltip_font_color', $post_id);
            ?>
            <!-- Template code goes here -->
                <style>
                    #lg-image-comparison-<?php echo $slug ?> .twentytwenty-handle{
                        background-color: <?php echo $handle_background; ?>;
                    }

                    #lg-image-comparison-<?php echo $slug ?> .before-label, #lg-image-comparison-<?php echo $slug ?> .after-label{
                        background-color: <?php echo $label_background_color; ?>;
                        color: <?php echo $label_font_color; ?>;
                    }

                    <?php if($tooltip_active): ?>
                    #lg-image-comparison-<?php echo $slug ?> .twentytwenty-tool-tip{
                        background-color: <?php echo $tooltip_background_color; ?>;
                        color: <?php echo $tooltip_font_color; ?>;
                    }
                    <?php endif; ?>
                </style>

                <div id="lg-image-comparison-<?php echo $slug ?>" class="lg-image-comparison">
                    <img src="<?php echo $image_before; ?>" />
                    <img src="<?php echo $image_after; ?>" />
                    <div class='twentytwenty-overlay'>
                        <div class='twentytwenty-before-label'>
                            <div class="before-label"><?php echo $image_before_label; ?></div>
                        </div>
                        <div class='twentytwenty-after-label'>
                            <div class="after-label"><?php echo $image_after_label; ?></div>
                        </div>
                    </div>
                    <div class='twentytwenty-handle'>
                        <span class='twentytwenty-left-arrow'></span>
                        <span class='twentytwenty-right-arrow'></span>
                        <?php if($tooltip_active): ?>
                        <div class='twentytwenty-tool-tip'>
                            <div><?php echo $tooltip_content; ?></div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>

                <script>
                    jQuery(window).load(function($) {
                        var element = jQuery("#lg-image-comparison-<?php echo $slug ?>");
                        element.twentytwenty();

                        element.find('.twentytwenty-handle').on("mousedown", function(event) {
                            jQuery(this).css('background-color', '<?php echo $handle_background_active; ?>');
                            <?php if($tooltip_active): ?>
                            element.find('.twentytwenty-tool-tip').css('display', 'none');
                            <?php endif; ?>
                            handleLock = true;
                        });
                        element.find('.twentytwenty-handle').on("mouseup", function(event) {
                            jQuery(this).css('background-color', '<?php echo $handle_background; ?>');
                            <?php if($tooltip_active): ?>
                            element.find('.twentytwenty-tool-tip').fadeIn();
                            <?php endif; ?>
                            handleLock = false;
                        });
                    });
                </script>
            <!-- end -->
        <?php

        return ob_get_clean();
    }
}

add_shortcode('lg-image-comparison', 'lg_image_comparison_shortcode');

add_action('edit_form_after_title', 'lg_image_comparison_post_edit_header');
function lg_image_comparison_post_edit_header() {
    if(get_post_type() == 'lg_image_comparison' && isset($_GET['post'])) {
        echo "<p style='color:red'>Paste below shortcode to template:<br>[lg-image-comparison id=". $_GET['post'] ."]<p>";
    }
}

?>
