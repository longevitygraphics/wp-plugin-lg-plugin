<?php
 
	class LG_image_comparison {
	    public function __construct() {
	    	/**** REFERENCE LG-includes/lg-cpt.php ****/
	        $image_comparison = new LG_cpt( 'lg_image_comparison', array(
	            'labels' => array(
	                'name' => __( 'LG-image-comparison' ),
	                'singular_name' => __( 'LG-image-comparison' )
	            ),
	            'public' => false,
	            'has_archive' => false,
	            'rewrite' => array('slug' => 'LG-image-comparison'),
	            'show_in_menu' => false
	        ) );

	        $this->image_comparison = $image_comparison;
	    }
	}

	$image_comparison           = new LG_image_comparison();
?>