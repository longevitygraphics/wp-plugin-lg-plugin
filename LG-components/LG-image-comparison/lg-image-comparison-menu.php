<?php
    add_action( 'admin_menu', 'lg_image_comparison_menu');
    function lg_image_comparison_menu() {

        // Add Map sub-menu
        add_submenu_page(
            $GLOBALS['lg_main_menu'], 
            'Image Comparison', 
            'Image Comparison', 
            'manage_options', 
            'edit.php?post_type=lg_image_comparison'
        );
    }
    
?>