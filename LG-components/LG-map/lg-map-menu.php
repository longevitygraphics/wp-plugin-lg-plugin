<?php
    add_action( 'admin_menu', 'lg_map_menu');
    function lg_map_menu() {

        // Add Map sub-menu
        add_submenu_page(
            $GLOBALS['lg_main_menu'], 
            'Map', 
            'Map', 
            'manage_options', 
            'edit.php?post_type=lg_map'
        );
    }
    
?>