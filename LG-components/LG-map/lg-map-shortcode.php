<?php
$lg_map_post_ids = array();
if ( ! function_exists( 'escape_string' ) ) {
	function escape_string( $string ) {
		return addslashes( $string );
	}
}

function lg_map_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'id' => 0,
		),
		$atts
	);

	if ( $atts['id'] == 0 ) {
		return 'Map does not exist';
	} else {

		wp_enqueue_style( 'lg-map-style' );
		wp_enqueue_script( 'lg-map-script' );

		global $lg_map_post_ids;
		$post_id = $atts['id'];
		$post    = get_post( $post_id );
		$slug    = $post->post_name;

		$lg_map_post_ids[] = $post_id;
		$width             = get_field( 'lg_map_width', $post_id );
		$height            = get_field( 'lg_map_height', $post_id );

		ob_start();
		?>
		<!-- Template code goes here -->
		<style>
			.lg-<?php echo $slug; ?> {
			<?php
			if ( $width ) :
				?>
				 width: <?php echo $width; ?>;
			<?php endif; ?>
						 <?php
							if ( $height ) :
								?>
				 height: <?php echo $height; ?>;
							<?php endif; ?>
			}
		</style>

		<div class="lg-map lg-<?php echo $slug; ?>">

		</div>


		<!-- end -->
		<?php

		return ob_get_clean();
	}
}

add_shortcode( 'lg-map', 'lg_map_shortcode' );

add_action( 'edit_form_after_title', 'lg_map_post_edit_header' );
function lg_map_post_edit_header() {
	if ( get_post_type() == 'lg_map' && isset( $_GET['post'] ) ) {
		echo "<p style='color:red'>Paste below shortcode to template:<br>[lg-map id=" . $_GET['post'] . ']<p>';
	}
}


function lg_map_script() {
	$post_ids = isset( $GLOBALS['lg_map_post_ids'] ) ? $GLOBALS['lg_map_post_ids'] : array();
	if ( $post_ids ) {
		?>
		<script>

			(function ($) {

				$(window).on('load', function () {
					<?php
					if ( count( $post_ids ) > 0 ) :
						foreach ( $post_ids as $post_id ) :
							$post = get_post( $post_id );
							$slug = $post->post_name;
							?>
					$('.lg-<?php echo $slug; ?>').each(function () {
						lg_<?php echo $post_id; ?>_map_initialize.call(this);
					});
							<?php
					endforeach;
					endif;

					?>
				});


				<?php
				if ( count( $post_ids ) > 0 ) :
					foreach ( $post_ids as $post_id ) :
						$map_center            = get_field( 'lg_map_center', $post_id );
						$map_zoom              = get_field( 'lg_map_zoom', $post_id );
						$marks_global_settings = get_field( 'marker_global_settings', $post_id );
						$marker_cluster        = get_field( 'marker_cluster', $post_id );

						$setting = get_field( 'settings', $post_id );
						$style   = get_field( 'lg_map_style', $post_id );
						?>
				var lg_<?php echo $post_id; ?>_map_initialize = function () {
					if (this) {
						var map_div = this,
							<?php if ( $setting == 'Auto' ) : ?>
							map_center = new google.maps.LatLng(-1, -1),
							<?php else : ?>
							map_center = new google.maps.LatLng( <?php echo $map_center['lat']; ?>, <?php echo $map_center['lang']; ?> ),
							<?php endif; ?>
							map_zoom = Number( <?php echo $map_zoom; ?> ),
							map = new google.maps.Map(this, {
								zoom: map_zoom,
								center: map_center,
								mapTypeId: google.maps.MapTypeId.ROADMAP,
								scrollwheel: false,
								<?php if ( $style ) : ?>
								styles: <?php echo $style; ?>
								<?php endif; ?>
							})

						<?php if ( have_rows( 'lg_map_markers', $post_id ) ) : ?>
						var infowindow = new google.maps.InfoWindow({
								maxWidth: 300
							}),
							marker,
							markersList = [],
							typeNames = [],
							i;

						var bounds = new google.maps.LatLngBounds();

							<?php
							while ( have_rows( 'lg_map_markers', $post_id ) ) :
								the_row();

								$lat                  = get_sub_field( 'lat' );
								$lang                 = get_sub_field( 'lang' );
								$icon                 = get_sub_field( 'icon' );
								$map                  = get_sub_field( 'map' );
								$info_window_template = get_sub_field( 'info_window_template' );

								if ( $info_window_template == 'Default' ) {
									$info_window_image = get_sub_field( 'info_window_image' );
									$info_window_copy  = get_sub_field( 'info_window_copy' );
									$copy              = "<div class='info-window'>" .
										"<img src='" . $info_window_image['url'] . "' alt='" . $info_window_image['alt'] . "'>" .
										"<div class='info-window-section'>" . $info_window_copy . '</div>' .
										'</div>';
									$copy              = formatStringForJs( $copy );
								} else {
									$copy = formatStringForJs( get_sub_field( 'info-window-content' ) );
								}

								?>
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(
								<?php echo $map['lat']; ?>,
								<?php echo $map['lng']; ?>
							),
							map: map,
								<?php if ( $icon ) : ?>
							icon: {
								url: "<?php echo $icon; ?>",
									<?php if ( $marks_global_settings['icon_width'] || $marks_global_settings['icon_height'] ) : ?>
								scaledSize: new google.maps.Size(<?php echo $marks_global_settings['icon_width']; ?>, <?php echo $marks_global_settings['icon_height']; ?>), // scaled size
								<?php endif; ?>
							},
							<?php elseif ( $marks_global_settings['icon'] ) : ?>
							icon: {
								url: "<?php echo $marks_global_settings['icon']; ?>",
								<?php if ( $marks_global_settings['icon_width'] || $marks_global_settings['icon_height'] ) : ?>
								scaledSize: new google.maps.Size(<?php echo $marks_global_settings['icon_width']; ?>, <?php echo $marks_global_settings['icon_height']; ?>), // scaled size
								<?php endif; ?>
							},
							<?php endif; ?>
						});

						bounds.extend(marker.position);

						markersList.push(marker);

								<?php if ( $info_window_template != 'None' && $copy && $copy != '' ) : ?>
						google.maps.event.addListener(marker, 'click', (function (marker, i) {
							return function () {
								infowindow.setContent("<?php echo $copy; ?>");
								infowindow.open(map, marker);
							}
						})(marker, i));
						<?php endif; ?>

							<?php endwhile; ?>

							<?php if ( $setting == 'Auto' ) : ?>
						if (markersList.length > 1) {
							map.fitBounds(bounds);
						} else if (markersList.length == 1) {
							map.setCenter({
								lat: markersList[0].position.lat(),
								lng: markersList[0].position.lng()
							});
						}
						<?php endif; ?>

							<?php if ( $marker_cluster == 1 ) : ?>
						var markerCluster = new MarkerClusterer(map, markersList, {imagePath: '<?php echo plugins_url( '/src/m/', __FILE__ ); ?>'});
						<?php endif; ?>
						<?php endif; ?>
					}
				}

						<?php
				endforeach;
				endif;

				?>
			}(jQuery));
		</script>
		<?php
	}
}

add_action( 'wp_footer', 'lg_map_script', 20 );
