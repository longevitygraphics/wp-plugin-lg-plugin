<?php
 
	class LG_map {
	    public function __construct() {
	    	/**** REFERENCE LG-includes/lg-cpt.php ****/
	        $map = new LG_cpt( 'lg_map', array(
	            'labels' => array(
	                'name' => __( 'LG-map' ),
	                'singular_name' => __( 'LG-map' )
	            ),
	            'public' => false,
	            'has_archive' => false,
	            'rewrite' => array('slug' => 'lg-map'),
	            'show_in_menu' => false
	        ) );

	        $this->map = $map;
	    }
	}

	$map           = new LG_map();
?>