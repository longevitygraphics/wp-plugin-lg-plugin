<?php 

add_action('admin_head', 'lg_map_remove_admin_editor');

function lg_map_remove_admin_editor() {

    remove_post_type_support('lg_map', 'editor');

}

function lg_map_scripts() {
	
	wp_register_script( 'lg-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBg2LuQqBPU81-m3Jo-xEdETVBeyMjA48M', array('jquery'), false, true );

	wp_enqueue_script( 'lg-map' );
}
add_action( 'wp_enqueue_scripts', 'lg_map_scripts' );

function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyBg2LuQqBPU81-m3Jo-xEdETVBeyMjA48M';
	
	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');