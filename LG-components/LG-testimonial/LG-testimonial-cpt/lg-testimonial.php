<?php

class LG_testimonial {
	public function __construct() {
		/**** REFERENCE LG-includes/lg-cpt.php */
		$testimonial = new LG_cpt(
			'lg_testimonial',
			array(
				'labels'       => array(
					'name'          => __( 'LG-testimonial' ),
					'singular_name' => __( 'LG-testimonial' ),
				),
				'public'       => false,
				'has_archive'  => false,
				'rewrite'      => array( 'slug' => 'lg-testimonial' ),
				'show_in_menu' => false,
				'show_in_rest' => true,
				'supports'     => array( 'editor', 'title', 'thumbnail' ),
			)
		);

		$this->testimonial = $testimonial;
	}
}

	$testimonial = new LG_testimonial();

