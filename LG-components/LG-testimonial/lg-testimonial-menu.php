<?php
add_action( 'admin_menu', 'lg_testimonial_menu' );
/**
 * Add the testimonial menu.
 */
function lg_testimonial_menu() {

	// Add collapse sub-menu.
	add_submenu_page(
		$GLOBALS['lg_main_menu'],
		'Testimonial',
		'Testimonial',
		'manage_options',
		'edit.php?post_type=lg_testimonial'
	);
}
