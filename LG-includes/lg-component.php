<?php

class LG_component {
    public $component_name;
    public $component_slug;
    public $component_path;
     
    /* Class constructor */
    public function __construct( $name, $slug, $path )
    {
        $this->component_name        = $name;
        $this->component_slug        = $slug;
        $this->component_path        = $path;
    }
}

?>