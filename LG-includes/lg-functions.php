<?php
/**
 * Format php string to javascript use
 *
 * @param String $str - input string
 *
 * @return String
 */
function formatStringForJs($str){
    return str_replace("\"", "'", preg_replace( "/\r|\n/", "", $str ));
}

/**
 * Remove all white space and make it lowcaser to compare
 *
 * @param String $str - input string
 *
 * @return String
 */
function stringToCompare($str){
	return strtolower(str_replace(' ', '', $str));
}

/**
 * Join Strings from ACF Repeater Field
 *
 * @param Array    $object         - Acf repeater object
 *		      String   $attr           - sub field attribute name.
 *
 * @return String
 */
function joinElementFromAcfRepeater($object, $attr){
    $string = '';

	$numItems = count($object);
	$i = 0;
    foreach ($object as $key) {
    	if(++$i === $numItems) {
		    $string .= $key[$attr];
		}else{
       		$string .= $key[$attr].' ';
		}
    }
    
    return $string;
}

/**
 * Join Strings from ACF Repeater Field
 *
 * @param Array   $object          - Acf repeater object
 *        String  $attr            - sub field attribute name.
 *
 * @return String
 */
 function hyphenToUnderscore($str){
    return str_replace('-', '_', $str);
 }

/**
 * Cut Html
 *
 * Cut html by format method.
 *
 * @param String  $String          - input string
 *        String  $formatType      - format string by 'word', 'char'
 *        Int     $formatLimit     - Limitation for the string output
 *
 * @return String
 */
function cutByChar($string, $limit){
  $chars = preg_replace( "/\r|\n/", " ", $string );
  $chars = strip_tags($chars);

  return substr($chars,0,$limit);
}

function cutByWord($string, $limit){
  $words = preg_replace( "/\r|\n/", " ", $string );
  $words = explode(' ',strip_tags($words));
  $words = array_slice($words, 0, $limit);

  return join(" ", $words);
}
function cutHtml($string, $formatType, $formatLimit){
  if(!$formatLimit || $formatLimit < 1){
    $formatLimit = 1;
  }
  switch ($formatType) {
      case "word":
          return cutByWord($string, $formatLimit);
          break;
      case "char":
          return cutByChar($string, $formatLimit);
          break;
      default:
          return cutByChar($string, $formatLimit);
  }
}

/**
 * Get Data
 *
 * Replace get_file_content()
 *
 * @param String  $String          - url
 *
 * @return String
 */
function lg_file_get_contents($url)
{
  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}
?>