// ----- gulp required var
var gulp = require('gulp'),
  jsmin = require('gulp-uglify'),
  path = require('path'),
  sass = require('gulp-sass'),
  rename = require('gulp-rename'),
  autoprefixer = require('gulp-autoprefixer'),
  minifycss = require('gulp-minify-css'),
  sourcemaps = require('gulp-sourcemaps');
concat = require("gulp-concat");

// ----- Path Configurations
var config = {
  sassPath: 'LG-components/',
  jsPath: 'LG-components/',
  nodePath: 'node_modules',
};

var dist = {
  cssPath: 'assets',
  jsPath: 'assets'
};

// ---- SASS Load Paths
var sassLoad = [
  config.nodePath + '/font-awesome/scss'
];

// Copy Fonts from npm font-awesome folder
gulp.task('font2font', function () {
  gulp.src(config.nodePath + '/font-awesome/fonts/**/*.*')
    .pipe(gulp.dest('./fonts'));
});

// ----- SASS
gulp.task('lg-image-comparison-styles', function () {
  return gulp.src(config.sassPath + '/LG-image-comparison/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('lg-image-comparison-style.min.css'))
    .pipe(minifycss())
    .pipe(gulp.dest(dist.cssPath));
});

gulp.task('lg-map-styles', function () {
  return gulp.src(config.sassPath + '/LG-map/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('lg-map-style.min.css'))
    .pipe(minifycss())
    .pipe(gulp.dest(dist.cssPath));
});


// ----- js min
gulp.task('lg-image-comparison-js', function () {
  gulp.src(config.jsPath + '/LG-image-comparison/**/*.js')
    .pipe(concat('lg-image-comparison-script.min.js'))
    .pipe(jsmin())
    .pipe(gulp.dest(dist.jsPath));
});
gulp.task('lg-map-js', function () {
  gulp.src(config.jsPath + '/LG-map/**/*.js')
    .pipe(concat('lg-map-script.min.js'))
    .pipe(jsmin())
    .pipe(gulp.dest(dist.jsPath));
});

// ----- Watch
gulp.task('watch', function () {
  gulp.watch(config.sassPath + '/**/*.scss', ['lg-image-comparison-styles', 'lg-map-styles']);
  gulp.watch(config.jsPath + '/**/*.js', ['lg-image-comparison-js', 'lg-map-js']);
});

// ----- Default
gulp.task('default', ['lg-image-comparison-styles', 'lg-map-styles', 'lg-image-comparison-js', 'lg-map-js', 'watch']);
