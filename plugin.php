<?php
/**
 * Plugin Name: LG-plugin
 * Plugin URI: https://www.longevitygraphics.com/
 * Author: Kelvin Xu,
 * Version: 2.2
 */
/* define environment variables */
$GLOBALS['lg_plugin_dir'] = plugins_url() . '/wp-plugin-lg-plugin/';
/**
 * Register the main menu page.
 */
function lg_init() {
	// Plugin Main
	require_once 'LG-includes/index.php';
	require_once 'LG-components/index.php';
}


/**********************
Assets
 ************************/

function lg_plugin_scripts() {
	wp_register_script(
		'lg-image-comparison-script',
		plugins_url( '/assets/lg-image-comparison-script.min.js', __FILE__ ),
		array( 'jquery' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'assets/lg-image-comparison-script.min.js' ),
		true
	);

	wp_register_script(
		'lg-map-script',
		plugins_url( '/assets/lg-map-script.min.js', __FILE__ ),
		array( 'jquery' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'assets/lg-map-script.min.js' ),
		true
	);

	wp_register_style(
		'lg-image-comparison-style',
		plugins_url( '/assets/lg-image-comparison-style.min.css', __FILE__ ),
		array(),
		filemtime( plugin_dir_path( __FILE__ ) . 'assets/lg-image-comparison-style.min.css' )
	);
	wp_register_style(
		'lg-map-style',
		plugins_url( '/assets/lg-map-style.min.css', __FILE__ ),
		array(),
		filemtime( plugin_dir_path( __FILE__ ) . 'assets/lg-map-style.min.css' )
	);
}

add_action( 'wp_enqueue_scripts', 'lg_plugin_scripts' );
/**********************
Dependencies checking
 ************************/
function lg_admin_notice__error( $message ) {
	$class = 'notice notice-error';

	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
}

function lg_plugin_dependencies_check() {
	if ( ! function_exists( 'get_plugins' ) ) {
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	}
	$plugins       = get_plugins();
	$error_message = '';

	if ( isset( $plugins['advanced-custom-fields-pro/acf.php'] ) ) {

		if ( ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
			$error_message .= 'LG Plugin : ACF Pro is not activated.';
		} elseif ( $plugins['advanced-custom-fields-pro/acf.php']['Version'] < 5.6 ) {
			$error_message .= 'LG Plugin : Please upgrade ACF to the newest version.';
		}
	} else {
		$error_message .= 'LG Plugin : ACF PRO is not detected';
	}

	if ( $error_message == '' ) {
		add_action( 'plugins_loaded', 'lg_init' );
	} else {
		add_action(
			'admin_notices',
			function() use ( $error_message ) {
				$class = 'notice notice-error';
				lg_admin_notice__error( $error_message );
			}
		);
	}
}

lg_plugin_dependencies_check();

