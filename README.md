# README #

Plugin Name: LG-plugin
Plugin Description: This plugin distribute components developed by Longevity Graphics to client.
Version 1.1 to 2.0

# Do not update plugin from 1.1 to 2.0 #


COMPONENT
---------------------------
1. Create a folder under LG-components
2. Code your component inside the folder and make sure its self-contained
3. Create php file 'Your component folder'/main.php
4. include component required files into 'Your component folder'/main.php

5. Define component in LG-components/index.php, example below:
	
	$GLOBALS['lg_component'] = array(
		/**** REFERENCE LG-includes/lg-component.php ****/
		new LG_component('Google Map', 'lg-map', 'LG-map/main.php')
	);

** Each component should have its own readme

THEME OPTIONS
-----------------------------


VERSION
-----------------------------
1.0
	- Basic Theme options, components, template override. Used on sites like exquisitehomedesign, skinmethod, etc

2.0
    - Move template override to Theme level. It's not managed by this plugin anymore. Please do not update plugin from 1.0 to 2.0
	
2.1
	- Integrate Form Tracking plugin to Main plugin.  Update from 2.0 is supported.
